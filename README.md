# Atelier API README

## Prerequisites

### To run the project you need:

  [Ruby v.3.1.2 with rbenv](https://github.com/rbenv/rbenv)

  [Rails 7.0.3](https://rubyonrails.org/2022/5/9/Rails-7-0-3-6-1-6-6-0-5-and-5-2-8-have-been-released)

  PostgreSQL 14.3

  ```shell
  sudo apt install postgresql-12
  ```
  
## Install

### Clone the repository

```shell
git clone git@gitlab.com:ukhanskyi_atelier/atelier_api.git
cd atelier_api
```

or

```shell
git clone https://gitlab.com/ukhanskyi_atelier/atelier_api.git
cd atelier_api
```

### Install dependencies

Using [Bundler](https://github.com/bundler/bundler):

```shell
bundle
```

### Initialize the database

```shell
rails db:create db:migrate db:seed
```

## Server

```shell
rails s
```

## Contributing
1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request :D
